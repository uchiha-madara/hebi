#!/usr/bin/python
# -*- coding: iso-8859-15 -*-

import argparse
import bisect
import locale

locale.setlocale(locale.LC_MONETARY, 'en_IN')

parser = argparse.ArgumentParser(
    description='Compare tax under old and new rates.')
parser.add_argument('income', type=int, help='Total income'
                    ' (including salary, rental income, etc.)')
parser.add_argument('-exemptions', type=int, default=0, help='Total exemptions'
                    ' (including standard deduction, 80C, NPS, medical, etc.)')
args = parser.parse_args()


def compute_tax(tax_slabs, tax_rates, taxable_income=0, cess_percent=4):
    # In this method, for the taxable income, first determine the lower
    # limit of the tax slab and the tax upto that limit. Then add the tax
    # on the part of the income above the limit.

    # bisect's 2nd argument should have one more element than 1st argument.
    # Since it returns index of the insertion point in the 2nd list,
    # the corresponding element in 1st list is at the previous index position.
    index = bisect.bisect(tax_slabs, taxable_income)
    lower_slab = tax_slabs[index - 1]
    lower_rate = tax_rates[index]

    more_tax = (taxable_income - lower_slab) * lower_rate[0]/100.0
    total_tax = more_tax + lower_rate[1]

    total_pay = total_tax * (1 + cess_percent/100.0)
    return total_pay


def get_old_tax(total_income, exempted_income=0):
    taxable_income = total_income - exempted_income
    if taxable_income <= 500000:
        return 0    # special case: tax rebate under 87A
    else:
        old_tax_slabs = [0, 250000, 500000, 1000000]
        old_tax_rates = [(0, 0), (0, 0), (5, 0), (20, 12500), (30, 112500)]

        return compute_tax(old_tax_slabs, old_tax_rates, taxable_income)


def get_new_tax(total_income):
    if total_income <= 500000:
        return 0    # special case: tax rebate under 87A
    else:
        new_tax_slabs = [0, 250000, 500000, 750000, 1000000, 1250000, 1500000]
        new_tax_rates = [(0, 0), (0, 0), (5, 0), (10, 12500), (15, 37500),
                         (20, 75000), (25, 125000), (30, 187500)]

        return compute_tax(new_tax_slabs, new_tax_rates, total_income)

old_tax = get_old_tax(args.income, args.exemptions)
new_tax = get_new_tax(args.income)

if old_tax < new_tax:
    choose = "OLD"
    summary = "You will pay " + locale.currency(new_tax - old_tax,
                                                grouping=True) + " less tax."
elif old_tax == new_tax:
    choose = "NEW"
    summary = "You will pay same tax, but you will have to do less work. ☺"
else:
    choose = "NEW"
    summary = "You will pay " + locale.currency(old_tax - new_tax,
                                                grouping=True) + " less tax."

print("Total tax, including cess\n"
      "    Under old rates: {} \n"
      "    Under new rates: {}\n"
      "\n"
      "You should choose the {:^5} tax rates.\n"
      "{}"
      .format(locale.currency(old_tax, grouping=True),
              locale.currency(new_tax, grouping=True), choose, summary))

